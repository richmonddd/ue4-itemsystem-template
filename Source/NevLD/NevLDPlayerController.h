// Copyright © 2016-2017, Claudio Parisi. All rights reserved.

#pragma once

#include "GameFramework/PlayerController.h"
#include "ItemSystem/Interactable.h"
#include "NevLDGameInstance.h"
#include "NevLDPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class NEVLD_API ANevLDPlayerController : public APlayerController
{
	GENERATED_BODY()
	
protected:

	virtual void SetupInputComponent() override;
	
	/** Interact action when the proper input is given */
	void Interact();

public:

	ANevLDPlayerController();

	/** The interactable currently visible by the player */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class AInteractable* InSightInteractable;

	/** The player's inventory */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory Management")
	TArray<FStorableRow> Inventory;

	/** The capacity of the player's inventory (i.e. maximum amount of inventory slots) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Inventory Management")
	uint8 InventoryCapacity;

	/** The capacity of the player's inventory (i.e. maximum amount of inventory slots) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inventory Management")
	bool bSpawnOnDrop;

	/** Store an Item in this Controller Inventory.
	* @return true if the Item has been stored, false otherwise
	*/
	UFUNCTION(BlueprintCallable, Category = "Inventory Management")
	bool StoreItem(FName ItemID);

	/** Drops an Item from this Controller Inventory.
	* @return true if the Item has been dropped, false otherwise
	*/
	UFUNCTION(BlueprintCallable, Category = "Inventory Management")
	bool DropItem(FName ItemID);

	/** Remove an Item from this Controller Inventory.
	* @return true if the Item has been removed, false otherwise
	*/
	UFUNCTION(BlueprintCallable, Category = "Inventory Management")
	bool RemoveItem(FName ItemID);

	/** Open/Closes the Inventory GUI. Must be implemented using Blueprint. */
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Inventory Management")
	void ToggleInventory();

	/** Reloads the Inventory GUI. Must be implemented using Blueprint. */
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Inventory Management")
	void ReloadInventory();

};
