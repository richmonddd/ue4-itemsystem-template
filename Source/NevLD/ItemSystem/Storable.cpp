// Copyright © 2016-2017, Claudio Parisi. All rights reserved.

#include "NevLD.h"
#include "NevLDPlayerController.h"
#include "Storable.h"


AStorable::AStorable() {
	ItemID = FName("Storable ID");
	Hint = "Pick up";
}

void AStorable::Use_Implementation(ANevLDPlayerController* Controller, bool bRemoveFromInventory)
{
	if(bRemoveFromInventory && Controller != nullptr && Controller->RemoveItem(ItemID))
		Destroy();
	//UE_LOG(LogTemp, Warning, TEXT("Unexpected Use of a Storable from base class, you should implement this through inheritance."));
}

void AStorable::Interact_Implementation(ANevLDPlayerController* Controller)
{
	//Once we are sure the Controller has stored this Storable, we remove it from the game world (destroy)
	if (Controller != nullptr && Controller->StoreItem(ItemID)) 
		Destroy();
}
