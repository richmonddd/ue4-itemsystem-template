// Copyright © 2016-2017, Claudio Parisi. All rights reserved.

#pragma once

#include "ItemSystem/Interactable.h"
#include "Storable.generated.h"

/**
* This class represents an Interactable a player can store in its Inventory.<br/>
* The Storable can also be usable.<br/>
* Examples are crafting ingredients, potions, etc.
*/
UCLASS()
class NEVLD_API AStorable : public AInteractable
{
	GENERATED_BODY()
	
public:

	AStorable();

protected:

	/** The FItemRow ItemID corresponding to this Storable */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FName ItemID;
	
	/** Called when we use this Storable.<br/>
	* In its base implementation, once a Storable is used it gets always destroyed.<br/>
	* If a Controller is provided and bRemoveFromInventory == true, it is also removed from the player's inventory, if found.<br/>
	* A Designer can choose to implement it using both blueprints or C++ (overriding the _Implementation)
	* @param Controller The Controller that has this Storable in its Inventory, if any
	* @param bRemoveFromInventory Whether this Storable will be removed from the Controller's Inventory once used.
	*/
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Item Functions")
	void Use(ANevLDPlayerController* Controller, bool bRemoveFromInventory);
	virtual void Use_Implementation(ANevLDPlayerController* Controller, bool bRemoveFromInventory);

	virtual void Interact_Implementation(ANevLDPlayerController* Controller) override;
};
