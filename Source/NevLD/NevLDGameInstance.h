// Copyright © 2016-2017, Claudio Parisi. All rights reserved.

#pragma once

#include "Engine/GameInstance.h"
#include "Engine/DataTable.h"
#include "NevLDGameInstance.generated.h"

/** Structure describing a row in the Item DataTable. */
USTRUCT(BlueprintType)
struct FStorableRow : public FTableRowBase {
	GENERATED_BODY()

public:

	FStorableRow() {
		Name = FText::FromString("Dummy Item");
		Storable = NULL;
		bUsable = false;
		bDestroyOnUse = true;
		Description = FText::FromString("Dummy Item Description");
		Value = 0;
	}

	/** The unique identifier of this item. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FName ItemID;
	
	/** The in-game name of this item. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FText Name;
	
	/** The thumbnail that'll be displayed in game (e.g. in the inventory) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UTexture2D* Thumbnail;

	/** Item implementation reference. (Subclass of Storable) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<class AStorable> Storable;

	/** Whether this item can be used by the player */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	bool bUsable;

	/** Whether this item is destroyed upon usage (Ignored if bUsable == false) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	bool bDestroyOnUse;

	/** The value of this item for the game economy system */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 Value;

	/** The in-game description of this item. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FText Description;
	
	/** Defines the equality among two items. */
	bool operator==(const FStorableRow Item) const {
		return ItemID == Item.ItemID;
	}

};

/**
 * A GameInstance can be useful when we need to manage resources throughout the game.<br>
 * It basically acts as a Singleton (and technically seems to be).<br>
 * For example we are using it as the only access point to load and store data to our DataTables.<br>
 * Eventually, can be used to instantiate and delete other Singletons if they're needed (not to be abused).
 */
UCLASS()
class NEVLD_API UNevLDGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:

	/** Retrieves a Storable using its ID */
	struct FStorableRow* GetStorableFromDataTable(FName ItemID);
	
protected:

	/** The DataTable containing all the Storables of the game */
	UPROPERTY(EditDefaultsOnly)
	UDataTable* StorableDataTable;
};
