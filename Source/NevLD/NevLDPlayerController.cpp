// Copyright © 2016-2017, Claudio Parisi. All rights reserved.

#include "NevLD.h"
#include "NevLDPlayerController.h"
#include "NevLDCharacter.h"
#include "ItemSystem/Storable.h"
#include "Engine.h"


ANevLDPlayerController::ANevLDPlayerController() {
	InventoryCapacity = 16;
	bSpawnOnDrop = false;
}

void ANevLDPlayerController::SetupInputComponent() {
	Super::SetupInputComponent();
	InputComponent->BindAction("Interact", IE_Pressed, this, &ANevLDPlayerController::Interact);
	InputComponent->BindAction("Inventory", IE_Pressed, this, &ANevLDPlayerController::ToggleInventory);
}

void ANevLDPlayerController::Interact()
{
	if (InSightInteractable != nullptr)
		InSightInteractable->Interact(this);
}

bool ANevLDPlayerController::StoreItem(FName ItemID)
{
	if (Inventory.Num() >= InventoryCapacity) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, TEXT("Inventory Full"));
		return false;
	}
	UNevLDGameInstance* GameInstance = Cast<UNevLDGameInstance>(GetWorld()->GetGameInstance());
	FStorableRow* Storable = GameInstance->GetStorableFromDataTable(ItemID);
	if (Storable != nullptr) {
		Inventory.Add(*Storable);
		ReloadInventory();
	}
	return true;
}

bool ANevLDPlayerController::DropItem(FName ItemID)
{
	if (bSpawnOnDrop) {
		ANevLDCharacter* Char = Cast<ANevLDCharacter>(GetCharacter());
		UNevLDGameInstance* GameInstance = Cast<UNevLDGameInstance>(GetWorld()->GetGameInstance());
		FStorableRow* StorableRow = GameInstance->GetStorableFromDataTable(ItemID);
		if (Char != nullptr && StorableRow != nullptr) {
			FVector Location = Char->GetActorLocation() + (Char->GetFollowCamera()->GetForwardVector() * 50);
			FRotator Rotation(0.0f, 0.0f, 0.0f);
			TSubclassOf<AStorable> StorableClass = StorableRow->Storable;
			if (StorableClass != NULL) {
				AStorable* Spawn = GetWorld()->SpawnActor<AStorable>(StorableClass, Location, Rotation);
				if (Spawn == nullptr) {
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Unable to spawn the item"));
				return false;
				}
			}
			else return false;
		}
		else return false;
	}
	return RemoveItem(ItemID);
}

bool ANevLDPlayerController::RemoveItem(FName ItemID)
{
	UNevLDGameInstance* GameInstance = Cast<UNevLDGameInstance>(GetWorld()->GetGameInstance());
	FStorableRow* Storable = GameInstance->GetStorableFromDataTable(ItemID);
	if (Storable != nullptr) {
		Inventory.RemoveSingle(*Storable);
		ReloadInventory();
	}
	return true;
}
